#7/29 update:
After playing with the webGL version (see the webgl branch), which is more up-to-date than the Canvas version, I ran into a GPU shader issue on Firefox 61.0.1 for Ubuntu, and after a quick Google search, apparently [this has been an issue since 2016](https://github.com/mrdoob/three.js/issues/9716). Apparently it's still a problem, and has been categorized as a browser issue for both Firefox and Safari. The Three.js devs can't really do anything about it. We may want to stick with the Canvas version instead (which is better because it already has the image we want to use, and it doesn't appear to be erroring out on my machine.)

#GFC Header Testing

I've included the basic three.js files and folder structure required to successfully run the HTML file located inside /index. Feel free to mess around and commit any cool changes!

-Matt
